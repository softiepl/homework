package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class PutCommentTest extends BaseCommentsTest{

    @Test
    void shouldReturnCode200WhenUpdateExistingComment() {

        int commentId = addNewComment();

        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("title", "edited Comment title");
        postBody.put("author", "edited Comment author");

        given().pathParam("id", commentId).
                and().contentType(ContentType.JSON).
                and().body(postBody).
                when().put(COMMENTS_URL + "/{id}").
                then().statusCode(200).
                and().body("title", is("edited Comment title")).
                and().body("author", is("edited Comment author"));
    }

}
