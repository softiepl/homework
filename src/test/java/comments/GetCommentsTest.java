package comments;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class GetCommentsTest extends BaseCommentsTest{

    @Test
    void shouldReturnCode200WhenGettingAllComments() {
        when().get(COMMENTS_URL).
                then().statusCode(200);
    }

    @Test
    void shouldReturnCode200WhenGettingSingleComment() {
        given().pathParam("id", "1").
                when().get(COMMENTS_URL+ "/{id}").
                then().statusCode(200).
                and().body("id", is(1)).
                and().body("body", is("some comment")).
                and().body("postId", is(1));
    }

}
