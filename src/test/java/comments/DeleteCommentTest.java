package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class DeleteCommentTest extends BaseCommentsTest{

    @Test
    void shouldReturnCode200WhenDeleteExistingComment() {

        int commentId = addNewComment();

        given().pathParam("id", commentId).
                when().delete(COMMENTS_URL + "/{id}").
                then().statusCode(200);
    }

}
