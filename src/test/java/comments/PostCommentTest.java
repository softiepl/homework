package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class PostCommentTest extends BaseCommentsTest{



    @Test
    void shouldReturn201StatusCodeWhenNewCommentIsCreated()
    {
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("title", "new Comment title");
        postBody.put("author", "new Comment author");

        given().contentType(ContentType.JSON).
                and().body(postBody).
                when().post(COMMENTS_URL).
                then().statusCode(201).
                and().body("title", is("new Comment title")).
                and().body("author", is("new Comment author"));
    }
}
