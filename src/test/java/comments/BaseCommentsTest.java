package comments;

import io.restassured.http.ContentType;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class BaseCommentsTest {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    protected int addNewComment() {
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("title", "new Comment title");
        postBody.put("author", "new Comment author");

        int newCommentId =
                given().contentType(ContentType.JSON).
                        and().body(postBody).
                        when().post(COMMENTS_URL).
                        then().statusCode(201).
                        and().extract().body().path("id");
        return newCommentId;
    }

}
